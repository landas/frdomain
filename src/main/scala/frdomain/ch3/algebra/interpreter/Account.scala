package frdomain.ch3
package algebra.interpreter

import java.awt.image.BandCombineOp
import java.util.{Calendar, Date}

import util.{Failure, Success, Try}

object common {
  type Amount = BigDecimal

  def today = Calendar.getInstance.getTime

  def test(): Unit = {
    val account = Account("1", "l")
    account.copy()
  }

  def main(args: Array[String]): Unit = {
    val a = Account("a1", "John")

    import AccountService._
    credit(a, 100).flatMap(debit(_, 100))
    Success(a.copy(balance = Balance(0 + 100))).flatMap(debit(_, 100))
    debit(Account("a1", "john", balance = Balance(100)), 100)
    Success(a.copy(balance = Balance(100 -100)))
    Success(Account("a1", "John", balance = Balance(0)))
  }
}

import common._

case class Balance(amount: Amount = 0)

case class Account(no: String, name: String, dateOfOpening: Date = today, dateOfClosing: Option[Date] = None,
                   balance: Balance = Balance(0))


