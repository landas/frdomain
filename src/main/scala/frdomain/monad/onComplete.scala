package frdomain.monad

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

object onComplete {
  implicit val context: ExecutionContextExecutor = ExecutionContext.global

  def calculateInterest(balance: BigDecimal): Future[BigDecimal] = Future {
    if (balance < 100) throw new Exception("at will")
    else BigDecimal(100)
  }

  def getCurrencyBalance(): Future[BigDecimal] = Future {
    BigDecimal(1000L)
  }

  def calculateNetAssetValue(ccyBalance: BigDecimal, interest: BigDecimal): Future[BigDecimal] = Future {
    ccyBalance + interest + 200
  }

  def main(args: Array[String]): Unit = {
    val result = for {
      b <- getCurrencyBalance()
      i <- calculateInterest(1)
      v <- calculateNetAssetValue(b, i)
    } yield (v)
    result onComplete {
      case Success(v) => println(v)
      case Failure(ex) => println(ex)
    }
  }
}
