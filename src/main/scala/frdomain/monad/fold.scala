package frdomain.monad

object fold {

  def main(args: Array[String]): Unit = {
    val s1 = SavingsAccount("dg", "sb001", 0.5)
    val s2 = SavingsAccount("sr", "sb002", 0.75)
    val s3 = SavingsAccount("ty", "sb003", 0.27)

    val period = DataRange()

    import frdomain.monad.AccountService.calculateInterest

    println("每个账户的余额", List(s1, s2, s3).map(calculateInterest(_, period))) // 对储蓄账户list映射并计算每个余额
    println("累计总利息", List(s1, s2, s3).map(calculateInterest(_, period)).foldLeft(BigDecimal(0))((a, e) => e.map(_ + a).getOrElse(a))) //
    println("", List(s1, s2, s3).map(calculateInterest(_, period)).filter(_.isSuccess))
  }

}