package frdomain.monad

import java.util.Date

import scala.util.{Failure, Success, Try}

object objImpl {
}

trait AccountService {

  def verifyCustomer(customer: Customer): Option[Customer] = {
    Some(customer).filter(Verifications.verifyRecord)
  }

  def openCheckingAccount(customer: Customer, effectiveDate: Date): Account = {
    val accountNo = ""
    val openingDate = new Date
    //. . ＋一一开户逻辑
    //    Account(accountNo, openingDate, customer.name, customer.address)
    SavingsAccount("", "", BigDecimal(1))
  }

  def calculateInterest[A <: InterestBearingAccount](account: A, period: DataRange): Try[BigDecimal] = {
//    Success(BigDecimal(10))
    Failure(new RuntimeException)
  }
}

object AccountService extends AccountService

case class DataRange()

case class Customer(name: String, address: String)

trait Account {
}

trait InterestBearingAccount extends Account {
  def rateOfInterest: BigDecimal
}

case class SavingsAccount(value: String, value2: String, rateOfInterest: BigDecimal) extends InterestBearingAccount

object Verifications {
  def verifyRecord(customer: Customer): Boolean = {
    true
  }
}

