package frdomain.monad

case class Tube[A](run: A) {
  def map[B](f: A => B): Tube[B] = Tube(f(run))

  def flatMap[B](f: A => Tube[B]): Tube[B] = f(run)
}

object test {
  def main(args: Array[String]): Unit = {
    val value: Tube[Int] = Tube(10)

    def add(a: Int, b: Int): Tube[Int] = {
      printf("a=>%d; b=>%d\n", a, b)
      Tube(a + b)
    }

    val f = for {
      a <- value
      b <- add(a, 3)
      c <- add(a, b)
      _ <- add(c, c)
    } yield c

    println(f) //Tube(23)
    println(f.run) //23
  }
}