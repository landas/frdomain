package frdomain.monad

import java.util.{Calendar, Date}

class copy {

  case class SavingsAccount(number: String, name: String, dateOfOpening: Date, rateOfInterest: BigDecimal)

  val today = Calendar.getInstance.getTime
  val a1 = SavingsAccount("a-123", "google", today, 0.1)
  val a2 = a1.copy(rateOfInterest = 0.2)
}
